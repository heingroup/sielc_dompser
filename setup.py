import sielc_dompser.__version__
from setuptools import setup

NAME = 'sielc_dompser'

setup(
    name=NAME,
    version=sielc_dompser.__version__.__version__,
    packages=['sielc_dompser'],
    url='https://gitlab.com/heingroup/sielc_dompser',
    author='Veronica Lai // Hein Group',
    description='This is an unofficial Python package to interface with SIELC Technologies, Inc products through DOMP (Device Objected Manager Protocol) serial communication. We are not affiliated with SIELC.',
    author_email='',
    python_requires='>=3.6',
)


